const socket = io("http://localhost:5000/");

/**
 * create a new color
 * @param {string} rgbList lista de colores en RGB ejem. #AA0,#0A0
 */
const saveColors = (rgbList) => {
  socket.emit("client:savecolors", {
    rgbList
  });
};

const form = document.querySelector("#form");
const rgblist = document.querySelector("#rgblist");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  saveColors(rgblist.value);
});

socket.on("server:loadcolors", (rgblist) => {
    console.log(rgblist);
    document.querySelector("#actual").innerHTML = rgblist;
});