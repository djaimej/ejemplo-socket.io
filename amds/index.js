const socket = io("http://localhost:5000/");
let interval = null;

socket.on("server:loadcolors", (rgblist) => {
    clearInterval(interval);
    console.log(rgblist);
    const ArrColors = rgblist.split(',');
    const ColorsLength = ArrColors.length - 1;
    let i = 0;
    var intervalHandle = setInterval((colors) => {
        $('main').css('background', colors[i]);
        i = i < ColorsLength ? i + 1 : 0;
    }, 1000, ArrColors);
    interval = intervalHandle;
});