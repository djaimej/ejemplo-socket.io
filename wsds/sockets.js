import { v4 as uuid } from "uuid";

let colors = null;

export default (io) => {
  io.on("connection", (socket) => {
    // console.log(socket.handshake.url);
    console.log("nuevo socket connectado:", socket.id);

    // Send all messages to the client
    socket.emit("server:loadcolors", colors);

    socket.on("client:savecolors", (list) => {
      colors = list.rgbList;
      console.log(`colores actuales ${colors}`);
      io.emit("server:loadcolors", colors);
    });

    socket.on("disconnect", () => {
      console.log(socket.id, "disconnected");
    });
  });
};
