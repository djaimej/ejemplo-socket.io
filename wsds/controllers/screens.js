import { v4 as uuid } from 'uuid';

let screens = [
];

export const getScreens = (req, res) => {
    console.log(`Screens in the database: ${JSON.stringify(screens)}`);

    res.send(screens);
}

export const generateScreen = (req, res) => {
    const Screen = {
        screenNumber: screens.length,
        colorList:[],
        id: uuid()
    };
    screens.push(Screen);
    console.log(`Screen ${Screen.id} added to the database.`);
    res.send(Screen);
};

export const getScreen = (req, res) => {
    const Screen = screens.find((Screen) => Screen.id === req.params.id);
    res.send(Screen);
};

export const updateScreen =  (req,res) => {
    const Screen = screens.find((Screen) => Screen.id === req.params.id);
    Screen.colorList = req.body.colorList.split(',');
    res.send(`Screen ${Screen.id} updated.`);
};