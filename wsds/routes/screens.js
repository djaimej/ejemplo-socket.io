import express from 'express';

import { generateScreen, getScreen, getScreens, updateScreen } from '../controllers/screens.js';

const router = express.Router();

router.get('/', getScreens);

router.post('/', generateScreen);

router.get('/:id', getScreen);

router.post('/:id', updateScreen);

export default router;